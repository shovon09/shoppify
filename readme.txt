Shoppify.
Team: DU_Avengers

Instruction to the Developers :
Use Intellij Idea.
Development Environment android 4.3.
JDK : 1.7
Framework : RoboGuice,Gson, ActionBarSherlock,Robo-Sherlock
ORM : OrmLight
Coding Convention : Standard Java Coding Conventions.
Build-Automation : Maven

Project structure Details :
1)activities : All the activities here.
2)adapter: The Adapters for custom list view.
3)app : Those classes which will be used throughout the application.
4)dao : Data access objects for the database.
5)domain : the entity classes for the db tables.
6)fragments: all the fragments here.
7)util : App utility here.


How to Clone Project for the first time :
1) Go to the git link and clone the project.
2) Select import project from the file in Intellij Idea.
3) Select the cloned directory.
4)Select create project from existing directory.
5)Press next without making any change until asked for project library or SDK setup.
6)Select android SDK(android 4.3) from the list.
7) next and finish.
8)That's it :D you are done.

Configuring Maven For the First time :
After successfully cloning and configuring project you need to configure maven to import the library.
 To do so find the pom.xml file and right click on it from the Intellij Idea.
 Then add it as a maven project.This will automatically import the necessary library.
 Then after importing is finished again right click on it and ignore this project.
 There will be an alert asking for ignoring the library also . You must choose "No".
 Now this should work properly.


