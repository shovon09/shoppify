
package com.du_avengers.shoppify.util.view;


public class SlidingMenuProperties {

	public int title;
	public int iconRes;
	public int counter;
	public boolean isHeader;

	public SlidingMenuProperties(int title, int iconRes, boolean header, int counter) {
		this.title = title;
		this.iconRes = iconRes;
		this.isHeader=header;
		this.counter=counter;
	}

	public SlidingMenuProperties(int title, int iconRes, boolean header){
		this(title,iconRes,header,0);
	}

	public SlidingMenuProperties(int title, int iconRes) {
		this(title,iconRes,false);
	}
}
