package com.du_avengers.shoppify.activity;

import android.os.Bundle;
import com.du_avengers.shoppify.R;
import com.du_avengers.shoppify.fragments.ProductsGridView;
import roboguice.fragment.RoboFragment;

/**
 * Home Activity
 */
public class MainActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheMainContent(R.id.content_frame, new ProductsGridView());
        initSlidingMenu();
    }

    @Override
    public void setTheMainContent(int layoutResourceId, RoboFragment fragment) {
        super.setTheMainContent(layoutResourceId, fragment);
    }

    @Override
    protected void initSlidingMenu() {
        super.initSlidingMenu();
    }
}