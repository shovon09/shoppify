package com.du_avengers.shoppify.activity;

import android.os.Bundle;
import android.view.MenuItem;
import com.du_avengers.shoppify.R;
import com.du_avengers.shoppify.fragments.SlidingMenuFragment;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import roboguice.activity.RoboFragmentActivity;
import roboguice.fragment.RoboFragment;
import roboguice.inject.ContentView;


/**
 * Created with IntelliJ IDEA.
 * User: Rifatul
 * Date: 8/16/13
 * Time: 10:00 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Base Activity for Sliding menu.
 * any activity that extends Base Activity must override the method setMainContent() and initSlidingMenu()
 */
@ContentView(R.layout.content_frame)
public class BaseActivity extends RoboFragmentActivity {
    private SlidingMenu menu;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void setTheMainContent(int layoutResourceId, RoboFragment fragment) {
        //  setContentView(R.layout.content_frame);
        getSupportFragmentManager().beginTransaction()
                .replace(layoutResourceId, fragment)
                .commit();
    }

    protected void initSlidingMenu() {
        menu = new SlidingMenu(this);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu_frame);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.menu_frame, new SlidingMenuFragment())
                .commit();
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setIcon(R.drawable.ic_navigation_drawer);
    }

    @Override
    public void onBackPressed() {
        if (menu.isMenuShowing()) {
            menu.showContent();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                menu.toggle();
        }
        return super.onOptionsItemSelected(item);
    }
}