package com.du_avengers.shoppify.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Rifatul
 * Date: 8/21/13
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProductInfo {
    private String productTitle;
    private String productPrice;
    private String imageUrl;

    public ProductInfo(String productTitle, String productPrice, String imageUrl) {
        this.productTitle = productTitle;
        this.productPrice = productPrice;
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "ProductInfo{" +
                "imageUrl='" + imageUrl + '\'' +
                ", productTitle='" + productTitle + '\'' +
                ", productPrice='" + productPrice + '\'' +
                '}';
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }
}
