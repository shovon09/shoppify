package com.du_avengers.shoppify.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.du_avengers.shoppify.R;
import com.du_avengers.shoppify.app.network.ImageLoader;
import com.du_avengers.shoppify.domain.ProductInfo;
import com.du_avengers.shoppify.gadget.ScaleImage;
import com.origamilabs.library.views.StaggeredGridView;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Rifatul
 * Date: 8/20/13
 * Time: 10:45 PM
 * To change this template use File | Settings | File Templates.
 */


public class GridViewAdapter extends ArrayAdapter<ProductInfo> {

    ArrayList<ProductInfo> products;
    private ImageLoader imageLoader;    //TODO:Replace ImageLoader with ProductInfo Loader.

    public GridViewAdapter(Context context, int resource, ArrayList<ProductInfo> objects) {
        super(context, resource, objects);
        imageLoader = new ImageLoader(context);
        this.products = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ProductInfo product = products.get(position);
        ProductViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(getContext());
            convertView = layoutInflator.inflate(R.layout.grid_view, null);
            holder = new ProductViewHolder(); //TODO: Delete View Holder .. no use :P .

            TextView productTitle = (TextView) convertView.findViewById(R.id.txtProductTitle);
            TextView productPrice = (TextView) convertView.findViewById(R.id.txtProductPrice);
            holder.imageView = (ScaleImage) convertView.findViewById(R.id.imageView1);
            productTitle.setText(product.getProductTitle());
            productPrice.setText(product.getProductPrice() + " BDT");

            //TODO:Set a progressing dialog/.gif image to the imageView.
            holder.imageView.setImageResource(R.drawable.spinner);
            convertView.setTag(holder);
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "Image Clicked", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            holder = (ProductViewHolder) convertView.getTag();
        }
        imageLoader.DisplayImage(product.getImageUrl(), holder.imageView);
        return convertView;
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    //TODO : Replace this ProductViewHolder Class with ProductInfo in the domain.
    static class ProductViewHolder {
        ScaleImage imageView;
    }

    class ProductClickListener implements StaggeredGridView.OnItemClickListener {

        @Override
        public void onItemClick(StaggeredGridView parent, View view, int position, long id) {
            switch (position) {
                case 0:
                default:
            }
        }
    }


}

