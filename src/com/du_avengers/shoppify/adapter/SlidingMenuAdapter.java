
package com.du_avengers.shoppify.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.du_avengers.shoppify.R;
import com.du_avengers.shoppify.util.view.SlidingMenuProperties;

public class SlidingMenuAdapter extends ArrayAdapter<SlidingMenuProperties> {

    public SlidingMenuAdapter(Context context) {
        super(context, 0);
    }

    public void addHeader(int title) {
        add(new SlidingMenuProperties(title, -1, true));
    }

    public void addItem(int title, int icon) {
        add(new SlidingMenuProperties(title, icon, false));
    }

    public void addItem(SlidingMenuProperties itemModel) {
        add(itemModel);
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).isHeader ? 0 : 1;
    }

    @Override
    public boolean isEnabled(int position) {
        return !getItem(position).isHeader;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SlidingMenuProperties item = getItem(position);
        ViewHolder holder = null;
        View view = convertView;

        if (view == null) {
            int layout = R.layout.ns_menu_row_counter;
            if (item.isHeader)
                layout = R.layout.ns_menu_row_header;

            view = LayoutInflater.from(getContext()).inflate(layout, null);

            TextView menuTitle = (TextView) view.findViewById(R.id.menurow_title);
            ImageView menuIcon = (ImageView) view.findViewById(R.id.menurow_icon);
            TextView menuCounter = (TextView) view.findViewById(R.id.menurow_counter);
            view.setTag(new ViewHolder(menuTitle, menuIcon, menuCounter));
        }

        if (holder == null && view != null) {
            Object tag = view.getTag();
            if (tag instanceof ViewHolder) {
                holder = (ViewHolder) tag;
            }
        }

        if (item != null && holder != null) {
            if (holder.textHolder != null)
                holder.textHolder.setText(item.title);

            if (holder.textCounterHolder != null) {
                if (item.counter > 0) {
                    holder.textCounterHolder.setVisibility(View.VISIBLE);
                    holder.textCounterHolder.setText("" + item.counter);
                } else {
                    holder.textCounterHolder.setVisibility(View.GONE);
                }
            }

            if (holder.imageHolder != null) {
                if (item.iconRes > 0) {
                    holder.imageHolder.setVisibility(View.VISIBLE);
                    holder.imageHolder.setImageResource(item.iconRes);
                } else {
                    holder.imageHolder.setVisibility(View.GONE);
                }
            }
        }

        return view;
    }

    public static class ViewHolder {
        public final TextView textHolder;
        public final ImageView imageHolder;
        public final TextView textCounterHolder;

        public ViewHolder(TextView text, ImageView image, TextView counter) {
            this.textHolder = text;
            this.imageHolder = image;
            this.textCounterHolder = counter;
        }
    }

}
