package com.du_avengers.shoppify.app.network;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.ImageView;
import com.du_avengers.shoppify.app.cache.FileCache;
import com.du_avengers.shoppify.app.cache.MemoryCache;
import com.du_avengers.shoppify.util.CachingUtil;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Using LazyList via https://github.com/thest1/LazyList/tree/master/src/com/fedorvlasov/lazylist
 * for the example since its super lightweight
 * I barely modified this file
 */
public class ImageLoader {
    /**
     * Var declaration.
     */
    MemoryCache memoryCache = new MemoryCache();
    FileCache fileCache;
    ExecutorService executorService;
    Handler handler = new Handler();//handler to display images in UI thread
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());

    public ImageLoader(Context context) {
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(5);
    }

    /**
     * Called to display image from adapter. This is lazy image loading.
     *
     * @param url
     * @param imageView
     */
    public void DisplayImage(String url, ImageView imageView) {

        imageViews.put(imageView, url);
        Bitmap bitmap = memoryCache.get(url);
        if (bitmap != null)
            imageView.setImageBitmap(bitmap);
        else {
            queuePhoto(url, imageView);
            imageView.setImageDrawable(null);    // TODO:EKhane loading animation diye dekhte pari.
        }
    }

    /**
     * @param url
     * @param imageView
     */
    private void queuePhoto(String url, ImageView imageView) {
        PhotoToLoad p = new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(p));
    }

    /**
     * @param url
     * @return
     */
    private Bitmap getBitmap(String url) {
        File imageFile = fileCache.getFile(url);
        //Check SD card for image.
        Bitmap imageBitmap = decodeFile(imageFile);
        if (imageBitmap != null)
            return imageBitmap;
        //Image Loading from web.
        try {
            Bitmap bitmapImage = null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream inputStream = conn.getInputStream();
            OutputStream outputStream = new FileOutputStream(imageFile);
            CachingUtil.CopyStream(inputStream, outputStream);
            outputStream.close();
           // inputStream.close();
            bitmapImage = decodeFile(imageFile);
            return bitmapImage;
        } catch (Throwable ex) {
            ex.printStackTrace();
            if (ex instanceof OutOfMemoryError)
                memoryCache.clear();
            return null;
        }
    }

    /**
     * decodes image and scales it to reduce memory consumption
     *
     * @param imageFile
     * @return
     */
    private Bitmap decodeFile(File imageFile) {
        try {
            //decode image size
            BitmapFactory.Options bitmapOption = new BitmapFactory.Options();
            bitmapOption.inJustDecodeBounds = true;
            FileInputStream fileInputStream = new FileInputStream(imageFile);
            BitmapFactory.decodeStream(fileInputStream, null, bitmapOption);
            fileInputStream.close();

            //Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 70;
            int widthTmp = bitmapOption.outWidth, heightTmp = bitmapOption.outHeight;
            int scale = 1;
            while (true) {
                if (widthTmp / 2 < REQUIRED_SIZE || heightTmp / 2 < REQUIRED_SIZE)
                    break;
                widthTmp /= 2;
                heightTmp /= 2;
                scale *= 2;
            }

            if (scale >= 2) {
                scale /= 2;
            }

            //decode with inSampleSize
            BitmapFactory.Options bitmapOption2 = new BitmapFactory.Options();
            bitmapOption2.inSampleSize = scale;
            FileInputStream fileInputStream1 = new FileInputStream(imageFile);
            Bitmap bitmapImage = BitmapFactory.decodeStream(fileInputStream1, null, bitmapOption2);
            fileInputStream1.close();
            return bitmapImage;
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param photoToLoad
     * @return
     */
    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    /**
     * Clear the cache.
     */
    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }

    /**
     * Task for the queue
     */
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;

        public PhotoToLoad(String url, ImageView imageView1) {
            this.url = url;
            imageView = imageView1;
        }
    }

    /**
     * PhotoLoader class Asynchronously load photos.
     */
    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            try {
                if (imageViewReused(photoToLoad))
                    return;
                Bitmap bitmapImage = getBitmap(photoToLoad.url);
                memoryCache.put(photoToLoad.url, bitmapImage);
                if (imageViewReused(photoToLoad))
                    return;
                BitmapDisplayer bitmapDisplayer = new BitmapDisplayer(bitmapImage, photoToLoad);
                handler.post(bitmapDisplayer);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    /**
     * Used to display bitmap in the UI thread
     */
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            if (bitmap != null)
                photoToLoad.imageView.setImageBitmap(bitmap);
            else
                photoToLoad.imageView.setImageDrawable(null);
        }
    }

}
