package com.du_avengers.shoppify.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.du_avengers.shoppify.R;
import com.du_avengers.shoppify.adapter.GridViewAdapter;
import com.du_avengers.shoppify.app.network.HttpRequestClient;
import com.du_avengers.shoppify.domain.ProductInfo;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.origamilabs.library.views.StaggeredGridView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Rifatul
 * Date: 8/15/13
 * Time: 8:47 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Product Grid View fragment. This fragment shows the product in a Grid View.
 */

//TODO : Some event Dispatcher Bug is present.
public class ProductsGridView extends RoboFragment {
    private static final int SUCCESS = 0;
    private static final String REQ_URL = "shoppify-web/json-data/get_latest_products.php?getLatestProduct=1";
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SUCCESS:
                    adapter = new GridViewAdapter(getActivity(), R.id.imageView1, products);
                    productGridView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    break;
                default:
                    break;
            }
        }
    };
    @InjectView(R.id.staggeredGridView1)
    private StaggeredGridView productGridView;
    private ArrayList<ProductInfo> products;
    private GridViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.grid_view_holder, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        int margin = getResources().getDimensionPixelSize(R.dimen.margin);
        productGridView.setItemMargin(margin);
        productGridView.setPadding(margin, 0, margin, 0);

    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            loadProduct(REQ_URL);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadProduct(String reqUrl) throws JSONException {
        HttpRequestClient.get(reqUrl,
                null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONObject jsonObject) {
                super.onSuccess(jsonObject);
                products = new ArrayList<ProductInfo>();
                Log.e("RES: ", "SUCCESS");

                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("latestProducts");
                    parseJSONArray(jsonArray);
                    handler.sendEmptyMessage(SUCCESS);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }
    private void parseJSONArray(JSONArray productArray) {
        int totalProduct = productArray.length();
        for (int i = 0; i < totalProduct; i++) {
            try {
                JSONObject product = productArray.getJSONObject(i);
                String productName = product.getString("productName");
                String productPrice = product.getString("fixedPrice");
                String productImage = product.getString("productImage");
                ProductInfo aProduct = new ProductInfo(productName, productPrice, productImage);
                products.add(aProduct);
                Log.e("PRODUCT INFO ", aProduct.toString());
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       // adapter.getImageLoader().clearCache();
    }
}
