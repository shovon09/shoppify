package com.du_avengers.shoppify.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.du_avengers.shoppify.R;
import com.du_avengers.shoppify.adapter.SlidingMenuAdapter;
import com.du_avengers.shoppify.util.view.SlidingMenuProperties;
import roboguice.fragment.RoboListFragment;

/**
 * Created with IntelliJ IDEA.
 * User: Rifatul
 * Date: 8/15/13
 * Time: 9:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class SlidingMenuFragment extends RoboListFragment {
    private String[] slidingMenuNames;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initMenu(); //Initializing the menu item.
    }

    public void initMenu() {
        SlidingMenuAdapter adapter = new SlidingMenuAdapter(getActivity());

        slidingMenuNames = getResources()
                .getStringArray(R.array.menu_items);
        String[] menuItemsIcon = getResources()
                .getStringArray(R.array.menu_items_icon);
        adapter.addHeader(R.string.menu_main_header);
        int res = 0;
        for (String item : slidingMenuNames) {
            int itemTitle = getResources().getIdentifier(item, "string",
                    getActivity().getPackageName());
            int itemIcon = getResources().getIdentifier(menuItemsIcon[res],
                    "drawable", getActivity().getPackageName());
            SlidingMenuProperties menuItem = new SlidingMenuProperties(itemTitle, itemIcon);
            if (res == 7) menuItem.counter = 12; //TODO:Counter for notification to be implemented
            if (res == 8) menuItem.counter = 3; //TODO:Counter for messages  to be implemented
            adapter.addItem(menuItem);
            res++;
        }
        adapter.addHeader(R.string.menu_main_header2);
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        //TODO:Sliding Menu action to be implemented
        switch (position) {
            case 0:
        }
    }
}
